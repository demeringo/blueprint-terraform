# Environment specific values.

# To load explicitely using CLI --var-file option
# eg: terraform plan --var-file terraform.production.tfvars

namespace = "my-terraform-blueprint-prod"
region    = "eu-west-1"
profile   = "default"
