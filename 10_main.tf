
module "networking" {
  source    = "./modules/networking"
  namespace = var.namespace
}

locals {
  common_tags = {
    Name        = "Something-common-changed"
    Environment = var.namespace
    Team        = var.team
  }
}

resource "aws_instance" "instance-in-my-public-subnet" {
  ami           = data.aws_ami.latest_ubuntu_ami.id
  instance_type = "t2.micro"
  metadata_options {
    http_tokens = "required"
  }
  tags = local.common_tags
}

resource "aws_instance" "other-intance" {
  ami           = data.aws_ami.latest_ubuntu_ami.id
  instance_type = "t2.micro"
  metadata_options {
    http_tokens = "required"
  }
  tags = local.common_tags
}

data "aws_ami" "latest_ubuntu_ami" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}
