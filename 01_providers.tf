provider "aws" {
  profile = var.profile
  region  = var.region

  # Working behind Coporate Proxy that rewrites SSL certs
  # See https://www.terraform.io/docs/provisioners/connection.html
  #
  # Prefered solution: point to the coporate certificate authority to trust
  # cacert =
  #
  # Workaround: uncomment following line to prevent SSL verification
  # insecure = "true"
}
