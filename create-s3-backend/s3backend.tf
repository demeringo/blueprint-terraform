/* Creates an S3 backend and DynamoDB table for Terraform.
  It is used to save and share Terraform state and implement the locking mechanism with multiple users.
 */
provider "aws" {
  region = "eu-west-1"
}
module "s3backend" {
  source    = "scottwinkler/s3backend/aws"
  namespace = "blueprint-terraform"
}
output "s3backend_config" {
  value = module.s3backend.config
}
