# Output variables from execution
output "vpc" {
  value = module.networking.vpc
}

output "sg" {
  value = module.networking.sg
}
