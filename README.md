# A terraform blueprint

A sample project to demonstrate terraform usage with AWS. Can be used as a blueprint to startup for a new infrastructure as code project.

Mainly inspired by work from Scott Winkler, author of the excellent _Terraform in action_ (https://www.manning.com/books/terraform-in-action).

See https://github.com/scottwinkler

- [A terraform blueprint](#a-terraform-blueprint)
  - [Infrastructure created](#infrastructure-created)
  - [Usage](#usage)
    - [Setup an S3 backend for remote state storage](#setup-an-s3-backend-for-remote-state-storage)
    - [Standard usage](#standard-usage)
  - [Pre-commit checks](#pre-commit-checks)
    - [Install pre-commit dependencies](#install-pre-commit-dependencies)
  - [Gitlab CI/CD](#gitlab-cicd)
    - [Required CI variables](#required-ci-variables)
  - [Module documentation](#module-documentation)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Infrastructure created

- S3 Storage for Terraform remote state and DynamoDB for locking
- A VPC
- 2 Subnets (public / private)
- EC2 instances in each subnets
- Security groups

## Usage

1. Create a terraform backend (to do only once, on first use)
2. Deploy infrastructure or update and redeploy

### Setup an S3 backend for remote state storage

> This is needed only the first time you setup the project

Edit `create-s3-backend/s3backend.tf` to your needs

- you may change region and namespace
- adapt the bucket key (name) to a unique name for your environment.

Create the backend

```sh
# Set AWS credentials
export AWS_ACCESS_KEY_ID=<YOUR_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<YOUR_SECRET>
export AWS_DEFAULT_REGION="eu-west-1"

cd create-s3-backend
# Optional: edit s3backend.tf

# Create backend
terraform init
terraform apply
```

```sh
# sample output after backend creation

Apply complete! Resources: 8 added, 0 changed, 0 destroyed.

Outputs:

s3backend_config = {
  "bucket" = "my-tf-backend-c3fcaktkt3-state-bucket"
  "dynamodb_table" = "my-tf-backend-c3fcaktkt3-state-lock"
  "region" = "eu-west-1"
  "role_arn" = "arn:aws:iam::814098365754:role/my-tf-backend-c3fcaktkt3-tf-assume-role"
}
```

Grab the output values and update `02_backend.tf` file with it.

> The backend creation scripts grants the correct role to the IAM user that is used to create it. If you need to allow another IAM user to deploy resources, you have to grant teh correct permissions to allow access this bucket.

### Standard usage

Once the Terraform backend is created, you can update infrastructure with:

```sh
# Set AWS credentials
export AWS_ACCESS_KEY_ID=<YOUR_ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<YOUR_SECRET>
AWS_DEFAULT_REGION="eu-west-1"

# Run init on first use
terraform init

# Deploy
terraform plan
terraform apply
```

Delete all resources previously created with

```sh
terraform destroy
```

## Pre-commit checks

You can optionally enforce local pre-commit checks (<https://pre-commit.com/>).

Checks are configured in `.pre-commit-config.yaml`

```sh
# Install pre-commit
pip install pre-commit
# On debian stable
#pip3 install pre-commit

# Test manual rules execution
pre-commit run --all-files
# Register pre-commit as a git hook to automate execution
pre-commit install
```

### Install pre-commit dependencies

You need to install dependencies for pre-commit checks.

- [detect-secrets](https://github.com/Yelp/detect-secrets)
- [Shellcheck](https://www.shellcheck.net/)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tfsec](https://github.com/tfsec/tfsec)
- [tflint](https://github.com/terraform-linters/tflint)

On windows

```powershell
choco install shellcheck
choco install terraform-docs
choco install tfsec
choco install tflint
```

On Linux

```sh
brew install shellcheck
brew install terraform-docs
brew install tfsec
brew install tflint
```

## Gitlab CI/CD

The `gitlab-ci.yml` file is used to

- automate static and dynamic checks
- aks for manual validation and trigger deployment

### Required CI variables

| gitlab variable name  | content              |
| --------------------- | -------------------- |
| AWS_ACCESS_KEY_ID     | aws key id           |
| AWS_SECRET_ACCESS_KEY | aws secret key value |

> In this project the aws region is hardcoded to eu-west-1 in .gitlab-ci.yml

## Module documentation

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.15 |
| aws | ~> 3.0 |
| null | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| namespace | The project namespace to use for unique resource naming. Something like project name + environment. | `string` | n/a | yes |
| profile | AWS profile to use | `string` | `null` | no |
| region | AWS region | `string` | `"us-west-2"` | no |
| ssh\_keypair | optional ssh keypair to use for EC2 instance | `string` | `null` | no |
| team | The team to contact for support | `string` | `"blueprint-team"` | no |

## Outputs

| Name | Description |
|------|-------------|
| sg | n/a |
| vpc | Output variables from execution |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
