terraform {
  backend "s3" {
    bucket         = "blueprint-terraform-c3fc-state-bucket"
    dynamodb_table = "blueprint-terraform-c3fc-state-lock"
    region         = "eu-west-1"
    role_arn       = "arn:aws:iam::814098365754:role/blueprint-terraform-c3fc-tf-assume-role"

    encrypt = true

    profile = "default"

    # Update key to have a tfstate file for your specific environment
    key = "environment1/terraform.tfstate"
  }
}
