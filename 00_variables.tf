# Holds the declaration of all input variables

variable "namespace" {
  description = "The project namespace to use for unique resource naming. Something like project name + environment."
  type        = string
}

variable "team" {
  description = "The team to contact for support"
  default     = "blueprint-team"
  type        = string
}

variable "ssh_keypair" {
  description = "optional ssh keypair to use for EC2 instance"
  default     = null
  type        = string
}

variable "profile" {
  description = "AWS profile to use"
  default     = null
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "us-west-2"
  type        = string
}
